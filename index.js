const express = require('express');

// cors 
const cors = require('cors');


const app = express();

// routes

const schoolroutes = require('./routes/schoolRoutes');

// cors
app.use(cors());
app.use(express.urlencoded({extended: true}));

// body parser
app.use(express.json());

// test route
app.get('/', function(req, res){
    
    res.json({
        message: 'Welcome to my API'
    });

});

// routes



app.use('/api/v1', schoolroutes.routes);






// listen for requests
app.listen(process.env.port || 4000, function(){

    console.log('App is listening for requests on localhost:4000');

});