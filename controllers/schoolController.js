const db = require("../models");

const School = db.schools;

const Op = db.Sequelize.Op;

// Create and Save a new School

const addSchool = async (req, res) => {

    let data = {
        School_Name: req.body.School_Name,
        website: req.body.website,
        School_Logo: req.body.School_Logo
    }

   const school =  await  School.create(data);

   res.json(school);




};

const getSchool = async (req, res) => {

    const school = await School.findAll();

    // attibutes: ['School_Name', 'website', 'School_Logo']

    

    res.json(school);

};


module.exports = {
    addSchool,
    getSchool
  };
  

