
module.exports = (sequelize, DataTypes) => {
    const School = sequelize.define('school', {
        School_Name: {
            type: DataTypes.STRING,
            allowNull: true
        },
        
        website: {
            type: DataTypes.STRING,
            allowNull: true
        },
        School_Logo: {
            type: DataTypes.STRING,
            allowNull: true
        },

    });
       
    return School;
}