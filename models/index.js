const dbconfig = require('../config/dbconfig');

const {Sequelize , DataTypes }= require('sequelize');

const sequelize = new Sequelize(dbconfig.DB, dbconfig.USER, dbconfig.PASSWORD, {
    host: dbconfig.HOST,
    dialect: dbconfig.dialect,
    operatorsAliases: 0,

    pool: {
        max: dbconfig.pool.max,
        min: dbconfig.pool.min,
        acquire: dbconfig.pool.acquire,
        idle: dbconfig.pool.idle
    }
});


sequelize.authenticate()

.then(() => {
    console.log('Connection has been established successfully.');
}
)
.catch(err => {
    console.error('Unable to connect to the database:', err);
}

);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.schools = require('./schoolModel.js')(sequelize, Sequelize);

db.sequelize.sync({force: false}).then(() => {
    console.log('Drop and Resync with { force: false }');
});


module.exports = db;

