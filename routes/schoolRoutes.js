// routes 

const express = require('express');
const router = express.Router();

const {
    addSchool,
    getSchool
} = require('../controllers/schoolController');



router.post('/addSchool', addSchool);
router.get('/getSchool', getSchool);
   


module.exports = {
    routes:router
}

